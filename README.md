# TRACT Coding Challenge

## Background

The main goal of interviews at TRACT is to find a good fit. We hope that you will enjoy working with us and be able to perform your best work in our environment! To this end, our interviews are not filled with gotchas and brainteasers, but attempt to let us work together in well-reasoned scenarios that mimic a collaborative environment.

## The Challenge

Georgie is a manager of a tomato distributor. Her company buys tomatoes and sells them on the market. Most of the time, tomatoes are bought from __farms__ and sold to the __stores__. However, sometimes tomato farms become __contaminated__, and tomatoes purchased from those farms can no longer be sold.

Georgie's boss asked her to build an App to summarize the business and __visualize__ the data.

She started by using [React](https://react.dev/) with [Shadcn](https://ui.shadcn.com/) and [Tailwind](tailwindcss.com/), and created some screens, but quickly ran into trouble.

## Your Goal

Georgie needs help to make progress on the App. You can decide on area(s) to focus on, and can augment or create new pages. Use your judgement and imagination! Here are some examples of things you could focus on:

- Visualizing the supply chain flow ( from farms to the stores )
- Handling the calculation of revenues and profits
- Identifying and estimating impact of contamination
- Fixing typescript errors and other bugs/issues
- Or choose your own direction!

## The Interview Format

You will pair with an interviewer who will observe you code through your solutions. The interviewer may ask questions or offer suggestions, but you will be guiding the direction of the work. You may use any reference materials (StackOverflow, Copilot, etc.) and tools that you use in your normal course of work. We believe that the best engineers will use references when needed, but have enough mastery of basic syntax, skills and best practices that they can work independently to a large measure. The main point is that we want you to feel comfortable and enjoy the interview, using your normal workflow!

## Prep Work

Please fork this repo and look through the code before your interview. You should also have an idea of areas of the code you will want to work on during the interview, so that we don't spend time during the interview trying to find work rather than coding.

While not required, it may help you to spend a few minutes coding in the project to see how it works. You may make any changes you'd like, including adding new packages or adding to or removing current code.

Just run `npm install && npm start` and you'll be on your way!

See you in the interview!
